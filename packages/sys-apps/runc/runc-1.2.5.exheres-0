# Copyright 2016 Marc-Antoine Perennou <marc-antoine.perennou@clever-cloud.com>
# Copyright 2016 Arnaud Lefebvre <arnaud.lefebvre@clever-cloud.com>
# Distributed under the terms of the GNU General Public License v2

require bash-completion
require github [ user=opencontainers tag=v${PV} ]

RUN_COMMIT=v${PV}-0-g7cb36325

SUMMARY="runc container cli tools"
DESCRIPTION="
runc is a CLI tool for spawning and running containers according to the OCI specification.
"
HOMEPAGE+=" https://www.opencontainers.org/"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS="
    man-pages [[ description = [ Build and install man pages ] ]]
    seccomp [[ description = [ Syscall filtering ] ]]
    selinux [[ description = [ selinux process and mount labeling ] ]]
"

# tests need to be run in docker
# Do not strip Go binaries
RESTRICT="test strip"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.22.4]
        man-pages? ( dev-golang/go-md2man )
    build+run:
        seccomp? ( sys-libs/libseccomp )
"

GOWORK=src/github.com/opencontainers

src_prepare() {
    default

    edo mkdir -p ${GOWORK}
    edo ln -s "${WORK}" ${GOWORK}/${PN}
}

src_compile() {
    local options=(
        $(optionq seccomp && echo seccomp)
        $(optionq selinux && echo selinux)
    )

    edo cd ${GOWORK}/runc
    emake COMMIT=${RUN_COMMIT} BUILDTAGS="$(echo ${options[@]})"

    optionq man-pages && edo man/md2man-all.sh
}

src_install() {
    dobin runc
    optionq man-pages && doman man/man8/*.8
    emagicdocs
    dobashcompletion contrib/completions/bash/runc
}

