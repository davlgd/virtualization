# Copyright 2012 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require pagure [ suffix=tar.xz ]
require vala [ vala_dep=true with_opt=true option_name=vapi ]
require meson
require test-dbus-daemon

SUMMARY="GObject based library API for managing information about operating systems"
HOMEPAGE+=" https://${PN}.org"

LICENCES="GPL-2 LGPL-2"
SLOT="1.0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    vapi [[ requires = [ gobject-introspection ] ]]

    ( providers: soup2 soup3 ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.9.0]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.7] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.10] )
    build+run:
        dev-libs/glib:2[>=2.44][gobject-introspection(+)?]
        dev-libs/libxml2:2.0[>=2.6.0]
        dev-libs/libxslt[>=1.0.0]
        providers:soup2? ( gnome-desktop/libsoup:2.4 )
        providers:soup3? ( gnome-desktop/libsoup:3.0 )
    run:
        sys-apps/osinfo-db-tools
    test+run:
        sys-apps/osinfo-db[>=20190905]
        sys-apps/hwdata
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-build-Add-option-to-select-libsoup-ABI.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    -Dwith-pci-ids-path=/usr/share/hwdata/pci.ids
    -Dwith-usb-ids-path=/usr/share/hwdata/usb.ids
)
MESON_SRC_CONFIGURE_OPTIONS=(
    'providers:soup3 -Dlibsoup-abi=3.0 -Dlibsoup-abi=2.4'
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    "gobject-introspection enable-introspection"
    "vapi enable-vala"
)
MESON_SRC_CONFIGURE_OPTION_ENABLES=(
    gtk-doc
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Denable-tests=true -Denable-tests=false'
)

